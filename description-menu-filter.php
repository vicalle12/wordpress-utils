<?php

// Filtro para mostrar descripcion menu - Añadir al functions.php
function twentyfifteen_nav_description( $item_output, $item, $depth, $args ) {
 	if ( $item->description ) {
  		$item_output = str_replace( $args->link_after . '</a>', '<div class="menu-item-description">' . $item->description . '</div>' . $args->link_after . '</a>', $item_output );
 	}

 	return $item_output;
}
add_filter( 'walker_nav_menu_start_el', 'twentyfifteen_nav_description', 10, 4 );
