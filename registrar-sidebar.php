<?php
function bridges_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'bridges' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
}
add_action( 'widgets_init', 'bridges_widgets_init' );